let ouvrir = function(e) {
	$(e.target).next().slideToggle("slow");
};

let fermer = function(e) {
	$(e.target).next().slideToggle("slow");
};

let init = function() {
	$("p.sub").mouseenter(ouvrir);
	$("p.sub").mouseleave(fermer);
};

export default {
	init: init
};