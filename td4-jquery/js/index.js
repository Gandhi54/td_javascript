import q1 from './texte.js';
import q2 from './boites.js';
import q3 from './slider.js';
import q4 from './menu.js';
import q5 from './fenetre.js';

$(document).ready(function() {
	q1.init();
	q2.init();
	q3.init();
	q4.init();
	q5.init();
});