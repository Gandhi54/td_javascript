let afficher = function(e) {
	if (e.target.id == "four"){
		showM('#fond');
	}
};

let showM = function(id) {
	let modal = $(id);
	modal.show();
	
	var winH = $(document).height();
	var winW = $(window).width();
	
	modal.css('top', winH/2 - modal.height()/2);
	modal.css('left', winW/2 - modal.width()/2);
};

let fermerM1 = function() {
	$('#fond').hide();
};

let init = function() {
	$('#four').click(afficher);
	$('.modal_close').click(fermerM1);
};

export default {
	init: init
};