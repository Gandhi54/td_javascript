let boiteUp = function(e) {
	if (e.target.classList.contains("boite_orange")){
		$(e.target).addClass('boite_orange_up');
	} 
	if (e.target.classList.contains("boite_verte")){
		$(e.target).addClass('boite_verte_up');
	}
};

let boiteDown = function(e) {
	if (e.target.classList.contains("boite_orange_up")){
		$(e.target).removeClass('boite_orange_up')
	} 
	if (e.target.classList.contains("boite_verte_up")){
		$(e.target).removeClass('boite_verte_up')
	}
};

let va = 5;
let ajoutBoite = function(e) {
	$("<div class=\"boite_orange\">"+va+"</div>").appendTo('#square');
	va++;
};

let change = function(e) {
	$('#square').children().toggleClass("boite_orange").toggleClass("boite_verte");		
};

let fadeTog = function(e) {
	$('#square').children().toggleClass("boite_orange").toggleClass("boite_orange_fade");
};

let init = function() {
	$('#square').mouseover(boiteUp);
	$('#square').click(boiteDown);
	$('#one').click(ajoutBoite);
	$('#two').click(change);
	$('#three').click(fadeTog);
};

export default {
	init: init
};