
//Exercice 1 
function logClick(){
	console.log("click");
}
window.addEventListener("load", () => {
	let a = document.getElementById("ex1");
	a.addEventListener("click", (event) => {
		logClick();
	});
});

//Exercice 2 
function updateButtonClass(e){
	if (e.classList.contains("c1")){
		e.className = "c2";
	} else {
		e.className = "c1";
	}
}

window.addEventListener("load", () => {
	let a = document.getElementById("ex1");
	a.addEventListener("click", (event) => {
		updateButtonClass(event.target);
	});
});

//Exercice 3
var nb = 1;
function addClick(){
	let s = document.getElementById("ex2");
	let b = document.createElement("LI");
	let t = document.createTextNode("Click button : "+nb); 
	b.appendChild(t);
	s.appendChild(b);	
	nb++;
}

window.addEventListener("load", () => {
	let a = document.getElementById("ex1");
	a.addEventListener("click", (event) => {
		addClick();
	});
});

//Exercice 4
function clearList() {
	document.getElementById("ex2").innerHTML="<li>Click</li>";
	nb = 1;
}

window.addEventListener("load", () => {
	let a = document.getElementById("ex3");
	a.addEventListener("click", (event) => {
		clearList();
	});
});

//Exercice 5
function incrInputValue() {
	let b = document.getElementById("ex4i");
	b.value++;	
}

window.addEventListener("load", () => {
	let a = document.getElementById("ex4b+");
	a.addEventListener("click", (event) => {
		incrInputValue();
	});
});

function decrInputValue() {
	let b = document.getElementById("ex4i");
	b.value--;	
}

window.addEventListener("load", () => {
	let a = document.getElementById("ex4b-");
	a.addEventListener("click", (event) => {
		decrInputValue();
	});
});


//Exercice 3 
var nb=0;
function insert(){
	nb+=1;
	document.getElementById("ex2").innerHTML += "<li>click bouton : "+nb+"</li>";
}
window.addEventListener("load", () => {
	let a3 = document.getElementById("ex1");
	a3.addEventListener("click", (event) => {
		insert();
	});
});

//Exercice 4 
function clearList(){
	document.getElementById("ex2").innerHTML = "<li>click</li>";
	nb=0;
}
window.addEventListener("load", () => {
	let a4 = document.getElementById("ex3");
	a4.addEventListener("click", (event) => {
		clearList();
	});
});

//Exercice 5 
function incrInputValue(){
	var val=Number(document.getElementById("ex4i").value);
	val +=1;
	document.getElementById("ex4i").value=val;
}
window.addEventListener("load", () => {
	let a5 = document.getElementById("ex4b");
	a5.addEventListener("click", (event) => {
		incrInputValue();
	});
});
