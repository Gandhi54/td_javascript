//Exercice 1
function max(x,y){
	if(x<y){
		return y;
	}else{
		return x;
	}
}

function min(x,y){
	if(x>y){
		return y;
	}else{
		return x;
	}
}


//question 1
function range(a,b){
	var mi = min(a,b);
	var ma = max(a,b);
	let t=[];
	for(x=mi;x<=ma;x++){
		t.push(x);
	}
	return t;
}


//question 2 version for
function sum(t){
var som=0;
	for ( let e of t) {
		som+=e;
	}
	return som;
}


//question 2 version foreach
function sum2(t){
var som=0;
	t.foreach((e) => {som+=e;});
	return som;
}


//question 2 version reduce
function sum2(t){
	return t.reduce((acc, elem ) => acc + elem, 0);
}


//question 3 : for
function moyenne(t){
  let moy = 0;
  for (let e of t){
    moy = moy + e;
  }
  return moy/t.length;
}


//question 3 : foreach
 function moyenne(t){
  let moy = 0;
  t.forEach((e) => { moy = moy + e })
  return moy/t.length;
}


//question 3 : reduce
function moyenne(t){ 
	let moy = (acc, elem) => acc + elem;
	let result = t.reduce(moy,0);
	return result/t.length;
}


//question 4
function tabchaine(t,pattern){
  let tab = new Array();
  t.forEach((e) => { tab.push(e + pattern.toUpperCase()) } )
  return tab;
}


//question 5
function filtermap(t, ft, tf){
	let r=[];
	t.forEach(function(e){
		if(ft(e)) r.push(tf(e));
	});
	return r; 
}
function transf(t,testFnct,transFnct) {
  let tab = new Array();
  for (let e of t){
	  if (testFnct(e)){
		  tab.push(transFnct(e,c));
	  }
  }
  return tab;
}

//question 6
function tabchaine(t,pattern){
  let verif = (element) => element != "";
  let trans = (element) => element + pattern.toUpperCase();
  let tab = t.filter(verif).map(trans);
  return tab;
}

//question 7
function refToUrl(t){
	let tab = new Array();
	t.forEach((e) => { tab.push("http://www.cata.log/products/"+e)})
	return tab;
}

//question 8
function refToUrl(t){
	let s = "<div class=catalogue><ul>"
	t.forEach((e) => { s = s+"<li> <a href=http://www.cata.log/products/"+e+">" + e + "</a></li>"})
	s = s + "</ul></div>" 
	return s;
}







//Exercice 2
//Question 1
function tabToMoy(t){
  let som = 0;
  t.forEach((e) => {som = som + e;});
  let moy = som / t.length;
  let o = {
    nbElements : t.length,
    somme : som,
    moy : moy
  }
  return o;
}

//Question 2
let etudiant = {
	numero : 15, 
	nom : "roton",
	prenom : "théo",
	dateNaiss = new Date(),
	mail: "théo.roton@hotmail.fr",
	notes : []
}

//Question 3
etudiant.prototype.calculAge = function() {
	let dateToday = new Date();
	let an = dateToday.getYear();
	let mois = dateToday.getMonth();
	let jour = dateToday.getDate();
	
	let anNaiss = this.dateNaiss.getYear();
	let moisNaiss = this.dateNaiss.getMonth();
	let jourNaiss = this.dateNaiss.getDate();
	
	let age;
	if (moisNaiss < mois){
		age = an - anNaiss;
	} else if (jourNaiss <= jour && moisNaiss == mois){
		age = an - anNaiss;
	} else {
		age = an - anNaiss - 1;
	}
	return age;
}

etudiant.prototype.afficherInfos = function() {
	console.log("Nom : "+this.nom.toUpperCase());
	console.log("Prénom : "+this.prenom);
	let dN = this.dateNaiss;
	let date = dN.getDate()+"/"+dN.getMonth()+"/"+dN.getFullYear();
	console.log("Date de naissance : "+date);
}

//Question 4
function Note(mat, val){
	this.matiere=mat;
	this.valeur=val;
}

etudiant.prototype.ajouterNote = function(matiere, note){
	let n = new Note(matiere,note);
	this.notes.push(n);
}


//Question 5
etudiant.prototype.calculMoy = function(){
	let gr = this.notes;
	let moy = 0;
	gr.forEach((e) => {moy = moy + e.valeur});
	return moy/gr.length;
}
	

//Question 6
function etudiant(num, n, pre, date, m){
	this.numero = num;
	this.nom = n;
	this.prenom = pre;
	this.dateNaiss=new Date(date);
	this.mail = m;
	this.notes=[];
}


//Question 7
function annivGroupe(t,m){
	let letu = new Array();
	t.forEach((e) => {
		if (e.dateNaiss.getMonth() == m){
			letu.push(e);
		}
	});
	return letu;
}

//Question 8
function ageDonne(t,age){
	let letu = new Array();
	t.forEach((e) => {
		if (e.calculAge() >= age){
			letu.push(e);
		}
	});
	return letu;
}

//Question 9
function Groupe(n,f,a){
	this.nomGpe = n;
	this.formation = f;
	this.liste = new Array();
	this.annee = a;
}

//Question 10
Groupe.prototype.ajouterEtu = function(e){
	this.liste.push(e);
}

Groupe.prototype.compterEtu = function(){
	return this.liste.length;
}

Groupe.prototype.moyenneGeneraleGroupe = function(){
	let moy = 0;
	this.liste.forEach((e) => {
		moy = moy + e.calculMoy();
	});
	return moy / this.liste.length;
}

//Question 11
Groupe.prototype.anniversaireGroupe = function(mat,mois){
	this.liste.forEach((e) => {
		if (e.dateNaiss.getMonth() == mois){
			e.notes.forEach((f) => {
				let matiere = f.matiere;
				if (matiere === mat){
					f.valeur = f.valeur + 2;
				}
			});
		}
	});
}

