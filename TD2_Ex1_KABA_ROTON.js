function max(x,y){
	if(x<y){
		return y;
	}else{
		return x;
	}
}

function min(x,y){
	if(x>y){
		return y;
	}else{
		return x;
	}
}


//question 1
function range(a,b){
	var mi = min(a,b);
	var ma = max(a,b);
	let t=[];
	for(x=mi;x<=ma;x++){
		t.push(x);
	}
	return t;
}


//question 2 version for
function sum(t){
var som=0;
	for ( let e of t) {
		som+=e;
	}
	return som;
}


//question 2 version foreach
function sum2(t){
var som=0;
	t.foreach((e) => {som+=e;});
	return som;
}


//question 2 version reduce
function sum2(t){
	return t.reduce((acc, elem ) => acc + elem, 0);
}


//question 3 : for
function moyenne(t){
  let moy = 0;
  for (let e of t){
    moy = moy + e;
  }
  return moy/t.length;
}


//question 3 : foreach
 function moyenne(t){
  let moy = 0;
  t.forEach((e) => { moy = moy + e })
  return moy/t.length;
}


//question 3 : reduce
function moyenne(t){ 
	let moy = (acc, elem) => acc + elem;
	let result = t.reduce(moy,0);
	return result/t.length;
}


//question 4
function tabchaine(t,pattern){
  let tab = new Array();
  t.forEach((e) => { tab.push(e + pattern.toUpperCase()) } )
  return tab;
}


//question 5
function filtermap(t, ft, tf){
	let r=[];
	t.forEach(function(e){
		if(ft(e)) r.push(tf(e));
	});
	return r; 
}
function transf(t,testFnct,transFnct) {
  let tab = new Array();
  for (let e of t){
	  if (testFnct(e)){
		  tab.push(transFnct(e,c));
	  }
  }
  return tab;
}

//question 6
function tabchaine(t,pattern){
  let verif = (element) => element != "";
  let trans = (element) => element + pattern.toUpperCase();
  let tab = t.filter(verif).map(trans);
  return tab;
}

//question 7
function refToUrl(t){
	let tab = new Array();
	t.forEach((e) => { tab.push("http://www.cata.log/products/"+e)})
	return tab;
}

//question 8
function refToUrl(t){
	let s = "<div class=catalogue><ul>"
	t.forEach((e) => { s = s+"<li> <a href=http://www.cata.log/products/"+e+">" + e + "</a></li>"})
	s = s + "</ul></div>" 
	return s;
}



