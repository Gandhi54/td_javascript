let name = "";
let life = 0;
let money = 0;
let awake = true;
	
function get(){
	return {
		name : name,
		life:life,
		money:money
		awake:awake
	}
}
	
function init(n, pv, arg){
	name = n;
	life = pv;
	money = arg; 
}

export(){
	init:init,
	get:get
}
