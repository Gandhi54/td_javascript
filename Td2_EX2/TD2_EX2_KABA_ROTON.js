//Question 1
function tabToMoy(t){
  let som = 0;
  t.forEach((e) => {som = som + e;});
  let moy = som / t.length;
  let o = {
    nbElements : t.length,
    somme : som,
    moy : moy
  }
  return o;
}

//Question 2
let etudiant = {
	numero : 15, 
	nom : "roton",
	prenom : "théo",
	dateNaiss = new Date(),
	mail: "théo.roton@hotmail.fr",
	notes : []
}

//Question 3
etudiant.prototype.calculAge = function() {
	let dateToday = new Date();
	let an = dateToday.getYear();
	let mois = dateToday.getMonth();
	let jour = dateToday.getDate();
	
	let anNaiss = this.dateNaiss.getYear();
	let moisNaiss = this.dateNaiss.getMonth();
	let jourNaiss = this.dateNaiss.getDate();
	
	let age;
	if (moisNaiss < mois){
		age = an - anNaiss;
	} else if (jourNaiss <= jour && moisNaiss == mois){
		age = an - anNaiss;
	} else {
		age = an - anNaiss - 1;
	}
	return age;
}

etudiant.prototype.afficherInfos = function() {
	console.log("Nom : "+this.nom.toUpperCase());
	console.log("Prénom : "+this.prenom);
	let dN = this.dateNaiss;
	let date = dN.getDate()+"/"+dN.getMonth()+"/"+dN.getFullYear();
	console.log("Date de naissance : "+date);
}

//Question 4
function Note(mat, val){
	this.matiere=mat;
	this.valeur=val;
}

etudiant.prototype.ajouterNote = function(matiere, note){
	let n = new Note(matiere,note);
	this.notes.push(n);
}


//Question 5
etudiant.prototype.calculMoy = function(){
	let gr = this.notes;
	let moy = 0;
	gr.forEach((e) => {moy = moy + e.valeur});
	return moy/gr.length;
}
	

//Question 6
function etudiant(num, n, pre, date, m){
	this.numero = num;
	this.nom = n;
	this.prenom = pre;
	this.dateNaiss=new Date(date);
	this.mail = m;
	this.notes=[];
}


//Question 7
function annivGroupe(t,m){
	let letu = new Array();
	t.forEach((e) => {
		if (e.dateNaiss.getMonth() == m){
			letu.push(e);
		}
	});
	return letu;
}

//Question 8
function ageDonne(t,age){
	let letu = new Array();
	t.forEach((e) => {
		if (e.calculAge() >= age){
			letu.push(e);
		}
	});
	return letu;
}

//Question 9
function Groupe(n,f,a){
	this.nomGpe = n;
	this.formation = f;
	this.liste = new Array();
	this.annee = a;
}

//Question 10
Groupe.prototype.ajouterEtu = function(e){
	this.liste.push(e);
}

Groupe.prototype.compterEtu = function(){
	return this.liste.length;
}

Groupe.prototype.moyenneGeneraleGroupe = function(){
	let moy = 0;
	this.liste.forEach((e) => {
		moy = moy + e.calculMoy();
	});
	return moy / this.liste.length;
}

//Question 11
Groupe.prototype.anniversaireGroupe = function(mat,mois){
	this.liste.forEach((e) => {
		if (e.dateNaiss.getMonth() == mois){
			e.notes.forEach((f) => {
				let matiere = f.matiere;
				if (matiere === mat){
					f.valeur = f.valeur + 2;
				}
			});
		}
	});
}






