//1
function creerMultiplicateur(n){
  return (x) => (x * n);
}
 
//2
function creerSequence(init,step){
  let x = init;
  return () => {
    let n = x + step;
    x = n;
    return n;
  }
}
  

//3
function fibonacci(u0,u1){
	let x = u0;
	let y = u1;
	return () => {
		let s = x + y;
		x = y;
		y = s;
		return s;
	}
}

//4
function creerMultiplicateur(n,x = null){
  if (!(x == null)){
    return n * x;
  } else if (x == null) {
        return (y) => (y * n);
  }
}

//5
function power(n,x = null){
  if (!(x == null)){
	if (n == 0) {
		return 1;
	} else {
		return power(n-1,x)*x;
		}
  } else if (x == null) {
		return (y) => {
			return power(y,n);
		}
	}
}

//6
function formatter(n0){
  let n = n0;
  return (s) => n++ + " : " + s;
}

//7
function write(s){
  console.log(s);
}

function writeAlert(s){
  alert(s);
}

//8
function logger(fF,fW){
  return (s) => {
                let form = fF(1);
                let writ = form(s);
                fW(writ);
  }
}
